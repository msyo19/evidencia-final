package com.example.msyo19.evdenciaFin;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;

public class Main2Activity extends AppCompatActivity {

    RecyclerView recyclerView;
    private DbPersona manager;
    Context c = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        manager = new DbPersona(this);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView= findViewById(R.id.recycler);
        recyclerView.setLayoutManager(llm);

        Adaptador adaptador= new Adaptador(getIntent().<Persona>getParcelableArrayListExtra("persona"));
        recyclerView.setAdapter(adaptador);

        DividerItemDecoration deco = new DividerItemDecoration(this,llm.getOrientation());
        recyclerView.addItemDecoration(deco);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.cerrar){
            finish();
        }
        if(id == R.id.ver){
            Intent intent= new Intent(c,Main2Activity.class);
            intent.putExtra("persona", (Serializable) manager.getPersonaList());
            finish();
            startActivity(intent);
        }
        if(id == R.id.borrar){
            manager.eliminarTodo();
            Intent intent= new Intent(c,Main2Activity.class);
            intent.putExtra("persona", (Serializable) manager.getPersonaList());
            finish();
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
