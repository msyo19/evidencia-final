package com.example.msyo19.evdenciaFin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DbPersona extends Manager {

        private static final String NOMBRE_TABLA="persona";
            //  n,app,apm,sexo,fecha,estado
        private static final String NOMBRE="n";
        private static final String APP="ap";
        private static final String APM="am";
        private static final String SEXO="s";
        private static final String FECHA="f";
        private static final String ESTADO="e";



        public static final String CREATE_TABLE = "create table " + NOMBRE_TABLA + " ("
                + NOMBRE + " text NOT NULL, "
                + APP + " text NOT NULL, "
                + APM + " text NOT NULL, "
                + SEXO + " text NOT NULL, "
                + FECHA + " text NOT NULL, "
                + ESTADO + " text NOT NULL "
                + ");";



        public DbPersona(Context ctx) {
            super(ctx);
        }


        @Override
        public void cerrar() {
            super.getDb().close();
        }



    private ContentValues generarContentValues(String n, String ap, String am, String s, String f, String e) {
            ContentValues valores = new ContentValues();
            valores.put(NOMBRE,n);
            valores.put(APP,ap);
            valores.put(APM,am);
            valores.put(SEXO,s);
            valores.put(FECHA,f);
            valores.put(ESTADO,e);

            return valores;
        }


        public void insertarParametros(String n, String ap, String am, String s, String f, String e) {
            super.getDb().insert(NOMBRE_TABLA, null, generarContentValues(n,ap,am,s,f,e));
        }



        @Override
        public void eliminarTodo() {
            super.getDb().execSQL("DELETE FROM "+ NOMBRE_TABLA+";");
        }

        @Override
        public Cursor cargarCursor() {
            return super.getDb().query(NOMBRE_TABLA, new String[]{"*"},null,null,null,null,null );
        }



        public List<Persona> getPersonaList(){
            List<Persona> list= new ArrayList();

            Cursor cursor= cargarCursor();


            while (cursor.moveToNext()){
                Persona persona = new Persona();
                persona.setNombre(cursor.getString(0));
                persona.setApp(cursor.getString(1));
                persona.setApm(cursor.getString(2));
                persona.setSexo(cursor.getString(3));
                persona.setFecha(cursor.getString(4));
                persona.setEstado(cursor.getString(5));
                list.add(persona);
            }

            return list;

        }


}
