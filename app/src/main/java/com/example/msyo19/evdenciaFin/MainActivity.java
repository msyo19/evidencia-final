package com.example.msyo19.evdenciaFin;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private DbPersona manager;
    Context c = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = new DbPersona(this);

        Button bo = (Button) findViewById(R.id.btn);
        bo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputLayout nombre = findViewById(R.id.Nom);
                TextInputLayout apellidop = findViewById(R.id.Appa);
                TextInputLayout apellidom = findViewById(R.id.Apma);
                TextView fecha = findViewById(R.id.fecha);
                Spinner estado = findViewById(R.id.Estado);
                RadioButton hombre = findViewById(R.id.hombre);
                RadioButton mujer = findViewById(R.id.mujer);
                String sexo;

                if(hombre.isChecked()){sexo = getString(R.string.mt);}else{sexo = getString(R.string.fdt);}
                //Persona persona = new Persona(Objects.requireNonNull(nombre.getEditText()).getText().toString(),Objects.requireNonNull(apellidop.getEditText()).getText().toString(),Objects.requireNonNull(apellidom.getEditText()).getText().toString(),sexo,fecha.getText().toString(),estado.getSelectedItem().toString());
                manager.insertarParametros(Objects.requireNonNull(nombre.getEditText()).getText().toString(),Objects.requireNonNull(apellidop.getEditText()).getText().toString(),Objects.requireNonNull(apellidom.getEditText()).getText().toString(),sexo,fecha.getText().toString(),estado.getSelectedItem().toString());
                Intent intent= new Intent(v.getContext(),Main3Activity.class);
                intent.putExtra("persona", (Serializable) manager.getPersonaList());
                startActivity(intent);
                Log.d("hola", manager.getPersonaList().get(0).getNombre());

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.cerrar){
            finish();
        }
        if(id == R.id.ver){
            Intent intent= new Intent(c,Main2Activity.class);
            intent.putExtra("persona", (Serializable) manager.getPersonaList());
            startActivity(intent);
        }
        if(id == R.id.borrar){
            manager.eliminarTodo();
        }
        return super.onOptionsItemSelected(item);
    }
}
